/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab01;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Lab01 {

    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'},};
    private static char currentPlayer = 'X';
    private static int row;
    private static int col;
    static char player1 = 'X';
    static char player2 = 'O';

    public static void main(String[] args) {
        printWelcome();
        printChooseCurrentPlayer();
        inputCurrentPlayer();
        while (true) {
            printTable();
            printTurn();
            inputRowCol();

            if (isWiner()) {
                printTable();
                printWin();
                break;
            } else if (isDraw()) {
                printTable();
                printDraw();
                break;
            }
            switchPlayer();
        }
        inputContinue();

    }

    private static void printWelcome() {
        System.out.println("|--------------------|");
        System.out.println("| Welcome To OX Game |");
        System.out.println("|--------------------|");
    }

    private static void printTable() {
        System.out.println("|-----------|");
        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " | ");
            }
            System.out.println("");
            System.out.println("|-----------|");
        }

    }

    private static void printTurn() {
        System.out.println("It's player " + currentPlayer + " turn.");
    }

    private static void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Please input row[1-3] and column[1-3] for your move [ex: 1 1] : ");
            row = sc.nextInt();
            col = sc.nextInt();
            if (table[row - 1][col - 1] == '-') {
                table[row - 1][col - 1] = currentPlayer;
                return;
            } else {
                System.out.println("Try Again");
            }
        }

    }

    private static void switchPlayer() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }

    private static boolean isWiner() {
        if (checkRow() || checkCol() || checkX1() || checkX2()) {
            return true;
        }
        return false;

    }

    private static void printWin() {
        System.out.println("Congratulation! Player " + currentPlayer + " wins!");
    }

    private static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[row - 1][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (table[i][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX1() {
        if (table[0][0] == currentPlayer && table[1][1] == currentPlayer && table[2][2] == currentPlayer) {
            return true;
        }
        return false;
    }

    private static boolean checkX2() {
        if (table[0][2] == currentPlayer && table[1][1] == currentPlayer && table[2][0] == currentPlayer) {
            return true;
        }
        return false;
    }

    private static boolean isDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    private static void printDraw() {
        System.out.println("The result is Draw!");
    }

    private static void printChooseCurrentPlayer() {
        System.out.println("|---------------------|");
        System.out.println("| Choose who go first |");
        System.out.println("|    Press 1 is X     |");
        System.out.println("|    Press 2 is O     |");
        System.out.println("|---------------------|");
    }

    private static void inputCurrentPlayer() {
        System.out.print("Choose here : ");
        Scanner sc = new Scanner(System.in);
        while(true){
            currentPlayer = sc.next().charAt(0);
            if(currentPlayer == '1'){
                currentPlayer = player1;
                System.out.println("X go first");
                break;
            }else if(currentPlayer == '2'){
                currentPlayer = player2;
                System.out.println("O go first");
                break;
            }else{
                System.out.println("Choose again : ");
            }
        }
    }

    private static void inputContinue() {
        Scanner sc = new Scanner(System.in);
        while(true){
            System.out.println("Do you want to play again? (y/n) : ");
            char ans = sc.next().charAt(0);
            if(ans == 'y'){
                resetGame();
                main(null);
                break;
            }else if(ans == 'n'){
                System.out.println("Thank you for playing! Goodbye!");
                break;
            }else{
                System.out.println("Invalid input. Please enter y or n ");
            }
        }
    }

    private static void resetGame() {
        table = new char[][]{{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
        currentPlayer = 'X' ;
    }

}
